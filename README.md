# cobox-mobile

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

- [About](#about)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`cobox-mobile` is cobox on Android.

## Install

Download the [latest release](https://github.com/okdistribute/cobox-mobile/releases)
APK file and [install manually](https://www.wikihow.com/Install-APK-Files-from-a-PC-on-Android) on your Android phone.

## Contributing

See [the contributing file](CONTRIBUTING.md)!

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

GPL-v3
